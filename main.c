/***************************************************************
* Montador IAS
* Autor: Samuel Toyoshi Ishida
* RA: 160250
***************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define MAX 4095

void report_error(char* msg, int line, int exit_code)
{
  fprintf(stderr, "ERROR on line %d\n", line);
  fprintf(stderr, "%s\n", msg);
  exit(exit_code);
}


typedef enum Diretivas{
	// Esta diretiva atribui um valor numerico a uma palavra
	set, //SYM               , HEX | DEC(0:231-1)

	// Esta diretiva informa ao montador o endereço de memória onde o montador deve iniciar (ou continuar) a geração do código
	org, // HEX | DEC(0:1023)

	// Esta diretiva informa ao montador para continuar a montagem a partir da próxima palavra com endereço múltiplo de N.
	align, // DEC(0:1023)

	// Esta diretiva preenche N palavras da memória com o dado D.
	wfill, // DEC(1:1023)	  , HEX | DEC(-231:231-1) | ROT | SYM

	// Esta diretiva coloca o valor N na memória
	word // HEX | DEC(0:2^32-1) | ROT | SYM
}Diretiva;

typedef char * String;
typedef int Flag;

typedef enum lado{ // Indica qual lado da palavra um rotulo se refere
	direita, esquerda
}Lado;

struct diretiva{
	Diretiva tipo;
	String arg1; // Parametro 1
	String arg2; // Parametro 2
};

typedef struct endereco{
	int n; // Endereco a ser atribuido ao rotulo
	Lado dir;  // Indica se esta a direita ou a esquerda
}Endereco;

struct rotulo{
	String nome;
	Endereco end;
}*listaDeRotulos[MAX]; // Inclui constantes definidas com .set

struct instrucao{
	int cod;
	String arg;
};

struct codigo{
	struct rotulo rotulo;
	struct diretiva diretiva;
	struct instrucao instrucao;
}codigo[MAX]; // Cada indice corresponde a uma linha do codigo dado

int tamMapa = 0;
int numRotulos = 0;

// Retorna o codigo de instrucao correspondente ao mnemonico fornecido
int codigoDeInstrucao(char *c)
{
	if (!strcmp(c, "LD"))
		return 0x01;
	else if (!strcmp(c, "LD-"))
		return 0x02;
	else if (!strcmp(c, "LD|"))
		return 0x03;
	else if (!strcmp(c, "ADD"))
		return 0x05;
	else if (!strcmp(c, "SUB"))
		return 0x06;
	else if (!strcmp(c, "ADD|"))
		return 0x07;
	else if (!strcmp(c, "SUB|"))
		return 0x08;
	else if (!strcmp(c, "LDmq_mx"))
		return 0x09;
	else if (!strcmp(c, "LDmq"))
		return 0x0A;
	else if (!strcmp(c, "MUL"))
		return 0x0B;
	else if (!strcmp(c, "DIV"))
		return 0x0C;
	else if (!strcmp(c, "LSH"))
		return 0x14;
	else if (!strcmp(c, "RSH"))
		return 0x15;
	else if (!strcmp(c, "JMP"))
		return 0x0D;
	else if (!strcmp(c, "JUMP+"))
		return 0x0F;
	else if (!strcmp(c, "ST"))
		return 0x21;
	else if (!strcmp(c, "STaddr"))
		return 0x12;
	else
		return -1; // Mnemonico invalido
}

int getDiretiva(char *c)
{
	if (!strcmp(c, ".org"))
		return org;
	else if (!strcmp(c, ".set"))
		return set;
	else if (!strcmp(c, ".word"))
		return word;
	else if (!strcmp(c, ".wfill"))
		return wfill;
	else if (!strcmp(c, ".align"))
		return align;
	else
		return -1; // Diretiva invalida
}

int cmpRotulo(const void* p1, const void* p2){
	return strcmp((*(struct rotulo **) p1)->nome,
		(*(struct rotulo **) p2)->nome);
}

// Faz uma busca binaria
Endereco getRotuloEnd(String str){
	int primeiro = 0;
	int ultimo = numRotulos - 1;
	int meio = (primeiro + ultimo) / 2;

	while (primeiro <= ultimo) {
		if (strcmp(listaDeRotulos[meio]->nome,str) < 0)
			primeiro = meio + 1;
		else if (!strcmp(listaDeRotulos[meio]->nome, str)) {
			return listaDeRotulos[meio]->end;
		}
		else
			ultimo = meio - 1;

		meio = (primeiro + ultimo) / 2;
	}

	return (Endereco){ -1, esquerda };
}

// Verifica se ha caracteres ilegais
int rotuloInvalido(String token){
	int i, tam = strlen(token);

	for (i = 0; i < tam - 1; i++)
	{
		if (token[i] == '.' && i == 0)
			continue;

		if (token[i] >= '0' && token[i] <= '9' && i == 0)
			return 1;

		if (!(token[i] >= 'a' && token[i] <= 'z')
			&& !(token[i] >= 'A' && token[i] <= 'Z')
			&& !(token[i] >= '0' && token[i] <= '9'))
			return 1;
	}

	return 0;
}


int main(int argc, const char* argv[])
{
	FILE *entrada, *saida;
	char linha[255], *token, str[15], palavra[4][7];
	int contLinhas = 0, aux1, aux2, i, j;
	char mapaDeMemoria[MAX][5][5]; // [0] = AAA, [1] = DD, [2] = DDD, [3] = DD, [4] = DDD
	Endereco end, p;
	struct rotulo *rot, *rot2;
	String endptr = malloc(15*sizeof(char));
	Flag rotulo, instrucao, diretiva;

	for (i = 0; i < MAX; i++){ // Inicializa os vetores
		codigo[i].diretiva.tipo = codigo[i].instrucao.cod = -1;
		codigo[i].rotulo.nome = NULL;
	}

	entrada = fopen(argv[1], "r");

	// Faz a leitura do codigo e organiza os dados nas estruturas de dados
	while (fgets(linha, sizeof(linha), entrada) != EOF)
	{
		rotulo = instrucao = diretiva = 0;

		if (linha[0] == '\n')
			continue;

        strcpy(endptr,"");

		token = strtok(linha, "\" \t,\n"); // Separa o token por espaco, tabulacao, aspas e virgulas

		while (token != NULL)
		{
			if (strstr(token, "#") != NULL) // Ignora o comentario
				break;

			if (token[0] == '.')// Entao se trata de uma diretiva
			{
				if (instrucao || diretiva)
				{
					sprintf(str, "Invalid line!");
					report_error(str, contLinhas, -1);
				}

				diretiva = 1;

				codigo[contLinhas].diretiva.tipo = getDiretiva(token);

				if (codigo[contLinhas].diretiva.tipo == -1){
					sprintf(str, "%s is not a valid directive!", token);
					report_error(str, contLinhas, -1);
				}

				// Pega arg1
				token = strtok(NULL, "\" \t,\n");
				strtol(token, &endptr, 0);

				if ((codigo[contLinhas].diretiva.tipo == set ||
					codigo[contLinhas].diretiva.tipo == word) &&
					rotuloInvalido(token) && strcmp(endptr,""))
				{
					sprintf(str, "%s is not a valid label!", token);
					report_error(str, contLinhas, -1);
				}

				else if (strcmp(endptr,"") &&
					codigo[contLinhas].diretiva.tipo != set &&
					codigo[contLinhas].diretiva.tipo != word)
				{
					if (token[0] == '0' && token[1] == 'x'){
						sprintf(str, "%s is not a valid hex number!", token);
						report_error(str, contLinhas, -1);
					}
					else
					{
						sprintf(str, "%s is not a valid number!", token);
						report_error(str, contLinhas, -1);
					}
				}

				codigo[contLinhas].diretiva.arg1 = malloc(sizeof(token));
				strcpy(codigo[contLinhas].diretiva.arg1, token);

				if (codigo[contLinhas].diretiva.tipo == wfill ||
					codigo[contLinhas].diretiva.tipo == set)
				{
					// Pega arg2
					token = strtok(NULL, "\" \t,\n");

					strtol(token, &endptr, 0);

					if (token[0] == '0' && token[1] == 'x' && strcmp(endptr,""))
					{
						sprintf(str, "%s is not a valid hex number!", token);
						report_error(str, contLinhas, -1);
					}

					else if (codigo[contLinhas].diretiva.tipo == set && strcmp(endptr,"")){
						sprintf(str, "%s is not a valid number!", token);
						report_error(str, contLinhas, -1);
					}
					else if (codigo[contLinhas].diretiva.tipo == wfill && strcmp(endptr,"") && rotuloInvalido(token))
					{
						sprintf(str, "%s is not a valid label!", token);
						report_error(str, contLinhas, -1);
					}

					//printf("%s\n", token);
					codigo[contLinhas].diretiva.arg2 = malloc(sizeof(token));
					strcpy(codigo[contLinhas].diretiva.arg2, token);
				}
			}

			else if (token[strlen(token) - 1] == ':') // Entao se trata de um rotulo
			{
				if (rotulo || diretiva || instrucao)
				{
					sprintf(str, "Invalid line!");
					report_error(str, contLinhas, -1);
				}

				rotulo = 1;

				token[strlen(token) - 1] = '\0';

				if (rotuloInvalido(token)){
					sprintf(str, "%s is not a valid label!", token);
					report_error(str, contLinhas, -1);
				}

				codigo[contLinhas].rotulo.nome = malloc(sizeof(token));
				strcpy(codigo[contLinhas].rotulo.nome, token);
			}

			else
			{
				if (diretiva || instrucao)
				{
					sprintf(str, "Invalid line!");
					report_error(str, contLinhas, -1);
				}

				instrucao = 1;

				aux1 = codigoDeInstrucao(token);

				if (aux1 == -1) // Comando invalido
				{
					if (codigo[contLinhas].diretiva.tipo == -1){
						sprintf(str, "%s is not a valid mnemonic!", token);
						report_error(str, contLinhas, -1);
					}
				}
				else if (aux1 == 0x14 || aux1 == 0x15) // LSH ou RSH nao tem arg
				{
					codigo[contLinhas].instrucao.cod = aux1;
					codigo[contLinhas].instrucao.arg = malloc(2 * sizeof(String));
					strcpy(codigo[contLinhas].instrucao.arg, "0");
					break;
				}
				else
				{
					codigo[contLinhas].instrucao.cod = aux1;

					// Le o endereco da instrucao
					token = strtok(NULL, "\" \t,\n");
					strtol(token, &endptr, 0);

					if (strcmp(endptr,"") && rotuloInvalido(token))
					{
						if (token[0] == '0' && token[1] == 'x'){
							sprintf(str, "%s is not a valid hex number!", token);
							report_error(str, contLinhas, -1);
						}
						else
						{
							sprintf(str, "%s is not a valid number or label!", token);
							report_error(str, contLinhas, -1);
						}
					}

					codigo[contLinhas].instrucao.arg = malloc(sizeof(token));
					strcpy(codigo[contLinhas].instrucao.arg, token);
				}
			}

			token = strtok(NULL, "\" \t,\n"); // Pega o proximo token, ate acabar a linha
		}

		contLinhas++;
	}

	// Remove do vetor os elementos nulos
	for (i = 0; i < contLinhas; i++)
	{
		if (codigo[i].diretiva.tipo == -1 &&
			codigo[i].instrucao.cod == -1 &&
			codigo[i].rotulo.nome == NULL){

			contLinhas--;

			for (j = i; j < contLinhas; j++)
				codigo[j] = codigo[j + 1];
		}
	}

	// Cria a lista de rotulos
	for (i = 0; i < contLinhas; i++){
		if (codigo[i].rotulo.nome != NULL)
			listaDeRotulos[numRotulos++] = &codigo[i].rotulo;
	}

	// O ponto de montagem inicial é o endereco 0 a esquerda

	// Primeiro atribui os enderecos dos rotulos
	for (i = 0, end = (struct endereco){ 0, esquerda }; i < contLinhas; i++)
	{

		// Se houver um rotulo naquela linha do codigo
		if (codigo[i].rotulo.nome != NULL)
			codigo[i].rotulo.end = (struct endereco) { end.n, end.dir };

		// Se houver uma diretiva na linha
		if (codigo[i].diretiva.tipo != -1)
		{
			if (codigo[i].diretiva.tipo == set)
			{
				listaDeRotulos[numRotulos] = malloc(sizeof(struct rotulo));
				listaDeRotulos[numRotulos]->nome = malloc(sizeof(codigo[i].diretiva.arg1));
				strcpy(listaDeRotulos[numRotulos]->nome, codigo[i].diretiva.arg1);
				listaDeRotulos[numRotulos++]->end = (struct endereco) { strtol(codigo[i].diretiva.arg2, NULL, 0), esquerda };
			}
			else if (codigo[i].diretiva.tipo == org)
			{
				end.n = strtol(codigo[i].diretiva.arg1, NULL, 0);
			}
			else if (codigo[i].diretiva.tipo == align)
			{
				aux1 = strtol(codigo[i].diretiva.arg1, NULL, 0);
				// Encontra o proximo endereco multiplo de N
				while (end.n % aux1 != 0)
					end.n++;
				end.dir = esquerda;
				continue;
			}
		}
		// Se trata de uma instrucao
		if (codigo[i].instrucao.cod != -1)
		{
			if (end.dir == esquerda)
				end.dir = direita;
			else{
				end.dir = esquerda;
				end.n++;
			}
		}

		else if (i < (contLinhas - 1) && end.dir == esquerda){
			if (codigo[i + 1].rotulo.nome != NULL ||
				((codigo[i + 1].diretiva.tipo == word  && codigo[i].rotulo.nome != NULL) ||
				(codigo[i + 1].diretiva.tipo == wfill && codigo[i].rotulo.nome != NULL)) && codigo[i].diretiva.tipo != -1)
				end.n++;
		}
	}

	// Ordena para poder fazer busca binaria
	qsort(listaDeRotulos, numRotulos, sizeof(struct rotulo *), cmpRotulo);

	// Monta o mapa de memoria
	for (i = 0, end = (struct endereco){ 0, esquerda }; i < contLinhas; i++)
	{
		if (codigo[i].diretiva.tipo != -1)
		{
			if (codigo[i].diretiva.tipo == org)
			{
				end = (struct endereco) { strtol(codigo[i].diretiva.arg1, NULL, 0), esquerda };
			}
			else if (codigo[i].diretiva.tipo == word)
			{
				sprintf(mapaDeMemoria[tamMapa][0], "%03x", end.n++);
				aux1 = strtol(codigo[i].diretiva.arg1, NULL, 0);

				if (aux1 == 0) // Entao o arg1 eh um rotulo
					aux1 = getRotuloEnd(codigo[i].diretiva.arg1).n;

				sprintf(str, "%010x", aux1);

				for (j = 0; j < 10; j++)
				{
					if (j < 2)
						palavra[0][j] = str[j];
					else if (j < 5)
						palavra[1][j - 2] = str[j];
					else if (j < 7)
						palavra[2][j - 5] = str[j];
					else
						palavra[3][j - 7] = str[j];
				}

				palavra[0][2] = palavra[1][3] = palavra[2][2] = palavra[3][3] = '\0';

				for (j = 0; j < 4; j++)
					sprintf(mapaDeMemoria[tamMapa][j + 1], "%s", palavra[j]);

				tamMapa++;
			}
			else if (codigo[i].diretiva.tipo == wfill)
			{
				for (aux2 = strtol(codigo[i].diretiva.arg1, NULL, 0); aux2 > 0; aux2--)
				{
					sprintf(mapaDeMemoria[tamMapa][0], "%03x", end.n++);
					aux1 = strtol(codigo[i].diretiva.arg1, NULL, 0);

					if (aux1 == 0) // Entao o arg1 eh um rotulo
						aux1 = getRotuloEnd(codigo[i].diretiva.arg1).n;

					sprintf(str, "%010x", aux1);

					for (j = 0; j < 10; j++)
					{
						if (j < 2)
							palavra[0][j] = str[j];
						else if (j < 5)
							palavra[1][j - 2] = str[j];
						else if (j < 7)
							palavra[2][j - 5] = str[j];
						else
							palavra[3][j - 7] = str[j];
					}

					palavra[0][2] = palavra[1][3] = palavra[2][2] = palavra[3][3] = '\0';

					for (j = 0; j < 4; j++)
						sprintf(mapaDeMemoria[tamMapa][j + 1], "%s", palavra[j]);

					tamMapa++;
				}
			}
			else if (codigo[i].diretiva.tipo == align)
			{
				aux1 = strtol(codigo[i].diretiva.arg1, NULL, 0);
				// Encontra o proximo endereco multiplo de N
				while (end.n % aux1 != 0)
					end.n++;
				end.dir = esquerda;
				continue;
			}

		}
		else if (codigo[i].instrucao.cod != -1)
		{
			aux1 = strtol(codigo[i].instrucao.arg, NULL, 0);

			// Se arg for um rotulo e a instrucao nao for LSH ou RSH
			if (aux1 == 0 && codigo[i].instrucao.cod != 0x14 && codigo[i].instrucao.cod != 0x15)
			{
				p = getRotuloEnd(codigo[i].instrucao.arg);

				if (p.dir == direita && ((codigo[i].instrucao.cod == 0x0D) ||
					(codigo[i].instrucao.cod == 0x0F) ||
					(codigo[i].instrucao.cod == 0x12)))
				{
					codigo[i].instrucao.cod++;
				}
			}

			if (end.dir == esquerda)
			{
				sprintf(mapaDeMemoria[tamMapa][0], "%03x", end.n);
				sprintf(mapaDeMemoria[tamMapa][1], "%02x", codigo[i].instrucao.cod);
				sprintf(mapaDeMemoria[tamMapa][2], "%03x", aux1);
				end.dir = direita;
			}
			else
			{
				sprintf(mapaDeMemoria[tamMapa][3], "%02x", codigo[i].instrucao.cod);
				sprintf(mapaDeMemoria[tamMapa][4], "%03x", aux1);
				end.dir = esquerda;
				end.n++;
				tamMapa++;
			}
		}
	}

	// Imprime a saida da maneira especificada
	if (argc == 2)
	{
		for (i = 0; i < tamMapa; i++)
		{
			printf("%s %s %s %s %s\n", mapaDeMemoria[i][0], mapaDeMemoria[i][1],
				mapaDeMemoria[i][2], mapaDeMemoria[i][3], mapaDeMemoria[i][4]);
		}
	}
	else if (argc == 3)
	{
		saida = fopen(argv[2], "w");
		for (i = 0; i < tamMapa; i++)
		{
			fprintf(saida, "%s %s %s %s %s\n", mapaDeMemoria[i][0], mapaDeMemoria[i][1],
				mapaDeMemoria[i][2], mapaDeMemoria[i][3], mapaDeMemoria[i][4]);
		}
		fclose(saida);
	}

	fclose(entrada);

	return 0; // Return OK
}
